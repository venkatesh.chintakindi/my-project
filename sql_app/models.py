
from datetime import datetime
from os import name
from matplotlib import docstring

from sqlalchemy import Column, Integer, Text, Binary, CHAR, DateTime, JSON, BIGINT,String
from sqlalchemy.sql.sqltypes import FLOAT
from sql_app.database import Base
class customer_details(Base):
    __tablename__ = "customer_details"
    __table_args__ = {'extend_existing': True}
    index=Column('index',Integer(),primary_key=True,autoincrement=True)
    email=Column('email',CHAR(20))
    password=Column('password',CHAR(20))
    