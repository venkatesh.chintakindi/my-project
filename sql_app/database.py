import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
import os, sys
host = '127.0.0.1'
port = 3306
username = 'root'
password = 'venky@123'
db = 'customer'
SQLALCHEMY_DATABASE_URL = "mysql+mysqlconnector://{}:{}@{}:{}/{}".format(
    username, password, host, port, db)
engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
db_session = scoped_session(SessionLocal)

Base = declarative_base()
Base.metadata.create_all(engine)