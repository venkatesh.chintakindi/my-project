from ctypes import Union
from fastapi import Form
from fastapi import FastAPI,Request
from pydantic import BaseModel
from fastapi.templating import Jinja2Templates
import re
import math
from typing import Union
templates = Jinja2Templates(directory="templates/")
app = FastAPI()

@app.get("/register") 
def index_page(request:Request):
    return templates.TemplateResponse('index.html',context={'request':request,'result':"please enter the base and exponent in the text fields given below"})
@app.post("/get-power")
def login(request:Request,base: Union[int,float,list] = Form(...), exponent: int = Form(...)):
    output_value=math.pow(base,exponent)
    print("output value of given base and power",output_value)
    return templates.TemplateResponse('index.html',context={'request':request,'result':output_value})  
