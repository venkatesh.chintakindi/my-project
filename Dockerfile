FROM python:3.9

COPY . /src

COPY ./requirements.txt /src/requirements.txt

WORKDIR src

EXPOSE 8085:8085
RUN pip install --upgrade pip

RUN pip install -r requirements.txt

CMD [ "uvicorn", "main:app", "--host", "0.0.0.0","--port","8085", "--reload" ]